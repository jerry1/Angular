import { Component, OnInit } from '@angular/core';

import { DataService } from "../data.service";
import { Item } from "../item";

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {
  items: Item[] = [];

  constructor(private data: DataService) { }

  ngOnInit(): void {
    this._getItems();
  }

  private _getItems(): void {
    this.data.getAllItems().subscribe(x => this.items = x);
  }

  removeItem(item: Item) {
    this.items = this.items.filter(x => x !== item);
    this.data.deleteItem(item.id).subscribe();
  }

  addItem(name: string) {
    this.data.addItem({ name } as Item).subscribe(x => {this.items.push(x);});
  }
}
