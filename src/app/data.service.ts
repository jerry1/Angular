import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {catchError, Observable, of, tap} from "rxjs";
import {Item} from "./item";
import {StockEntry} from "./stock-entry";

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private stockUrl = 'api/inStock';
  private itemUrl = 'api/items'

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json'})
  };

  constructor(
    private http: HttpClient) { }

  getAllStock(): Observable<StockEntry[]> {
    return this.http.get<StockEntry[]>(this.stockUrl).pipe(
      catchError(this.handleError<StockEntry[]>('getAllStock', []))
    );
  }

  getStock(id: number): Observable<StockEntry> {
    const url = `${this.stockUrl}/${id}`;
    return this.http.get<StockEntry>(url).pipe(
      catchError(this.handleError<StockEntry>(`getStock id:${id}`))
    );
  }

  getItem(id: number): Observable<Item> {
    const url = `${this.itemUrl}/${id}`;
    return this.http.get<Item>(url).pipe(
      catchError(this.handleError<Item>(`getItem id:${id}`))
    );
  }

  getAllItems(): Observable<Item[]> {
    return this.http.get<Item[]>(this.itemUrl).pipe(
      catchError(this.handleError<Item[]>('getAllItems', []))
    );
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    }
  }


  updateStock(stock: StockEntry): Observable<any> {
    return this.http.put(this.stockUrl, stock, this.httpOptions).pipe(
      tap(_ => console.log('saving..')),
      catchError(this.handleError<any>('updateStock'))
    );
  }

  addStock(entry: StockEntry): Observable<StockEntry> {
    return this.http.post<StockEntry>(this.stockUrl, entry, this.httpOptions).pipe(
      tap( _ => console.log('adding new stock ')),
      catchError(this.handleError<StockEntry>('addStock'))
    );
  }

  addItem(item: Item): Observable<Item> {
    return this.http.post<Item>(this.itemUrl, item, this.httpOptions).pipe(
      tap(_ => console.log('addItem..')),
      catchError(this.handleError<Item>('addItem'))
    );
  }

  deleteItem(id: number) {
    const url = `${this.itemUrl}/${id}`;
    return this.http.delete<Item>(url, this.httpOptions).pipe(
      tap(_ => console.log('deleteItem..')),
      catchError(this.handleError<Item>('deleteItem'))
    );
  }
}
