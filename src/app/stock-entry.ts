export interface StockEntry {
  id: number;
  count: number;
}
