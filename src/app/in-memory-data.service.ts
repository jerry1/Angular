import { Injectable } from '@angular/core';
import { InMemoryDbService } from "angular-in-memory-web-api";

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

  createDb() {
    const inStock = [
      {id: 1, count: 10 },
      {id: 2, count: 9 }
    ];
    const items = [
      {id: 1, name: 'Lentils' },
      {id: 2, name: 'Rolled Oats' },
      {id: 3, name: 'Tomatoes' }
    ];
    return { inStock, items };
  }



}
