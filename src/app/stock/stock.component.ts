import { Component, OnInit } from '@angular/core';
import { FormControl } from "@angular/forms";
import { map, Observable, startWith } from "rxjs";

import { DataService } from "../data.service";
import { Item } from "../item";
import { StockEntry } from "../stock-entry";

@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.scss']
})
export class StockComponent implements OnInit {
  items: Item[] = [];
  stockList: StockEntry[] = [];
  autoCompleteControl = new FormControl<string | Item>('');
  filteredItems!: Observable<Item[]>;

  constructor(private data: DataService) {}

  ngOnInit(): void {
    this._getStock();
    this.filteredItems = this.autoCompleteControl.valueChanges.pipe(
      startWith(''),
      map(value => {
        const name = typeof value === 'string' ? value : value?.name;
        return name ? this._filter(name as string) : this._filter('');
      })
    );
  }

  private _getStock(): void {
    this.data.getAllStock().subscribe(x => this.stockList = x);
    this.data.getAllItems().subscribe(x => {
      this.items = x;
      this.autoCompleteControl.setValue('');
    });
  }

  getItem(id: number): Item {
    let item = this.items.find(x => x.id == id) ;
    return item as Item;
  }

  modifyStock(stock: StockEntry, amount: number) {
    stock.count += amount;
    this.data.updateStock(stock).subscribe();
  }

  displayAutoComplete(item: Item): string {
    return item && item.name ? item.name : '';
  }

  private _filter(name: string): Item[] {
    const filterValue = name.toLowerCase();
    return this.items.filter(item => item.name.toLowerCase().includes(filterValue) && !this.isItemAlreadyInStock(item));
  }

  addStock(value: string | Item | null) {
    let item = typeof value === 'string' ? this.items.find(x => x.name == value) : value;
    if (!item || item == undefined) {
      console.log(`addStock attempted but unable to parse item ${value}`);
      return;
    }

    if (this.isItemAlreadyInStock(item)) {
      console.log('addStock duplicate stock attempted, ignoring');
      return;
    }

    this.data.addStock({ id: item.id, count: 1 } as StockEntry).subscribe(x => {this.stockList.push(x);});
  }

  isItemAlreadyInStock(item: Item):  boolean {
    return this.stockList.find(x => x.id == item.id) != null;
  }


  isItem(value: any): boolean {
    if (typeof(value) === 'string' || !value) {
      return false;
    }
    return value.name != undefined && value.id != undefined;
  }
}
